<link href="css/bootstrap.css" rel="stylesheet" />

<script src="js/bootstrap.js"></script>
<script src="js/jquery-3.6.0.js"></script>
<script src="js/private/novoagendamento.js"></script>
<?php


//   prof_id='+profissional_id+'&espe_id='+especialidade_id;

$prof_id = isset($_GET['prof_id']) ? $_GET['prof_id'] : '';
$espe_id = isset($_GET['espe_id']) ? $_GET['espe_id'] : '';

?>

<div class="modal fade" id="modal_exito" role="dialog">
    <div class="modal-dialog">           
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Feegow</h4>
            </div>
            <div class="modal-body">
            <h5 class="modal-title">Agendamento salvo com sucesso!!</h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="modal_preencher" role="dialog">
    <div class="modal-dialog">           
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Feegow</h4>
            </div>
            <div class="modal-body">
            <h5 class="modal-title">Preencha todos os campos!!</h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="modal_error" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Feegow</h4>
            </div>
            <div class="modal-body">

                <h5 class="modal-title">Aconteceu algum problema ao salvar!!</h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>

    </div>
</div>
<div class="modal fade" id="modal_origem" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Feegow</h4>
            </div>
            <div class="modal-body">

                <h5 class="modal-title">Escolha uma opção - como Conheceu?? </h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>

    </div>
</div>



<br>
<br>
<br>
<h3><span class="label label-default">Preencha seus dados</span></h3>
<br>
<br>
<div class="row">
    <div class="col-xl-4">
        <div class="form-group">
            <input type="text" class="form-control" id="txtnome" placeholder="Nome completo">
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group">
            <select id="cmbConheceu" class="form-select">
                <!-- <option value ='1'>Como Conheceu?</option> -->
            </select>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-xl-4">
        <div class="form-group">
            <input type="text" class="form-control" id="txtnascimento" placeholder="Nascimento">
        </div>
    </div>
    <div class="col-xl-4">
        <div class="form-group">
            <input type="text" class="form-control" id="txtcpf" placeholder="CPF">
        </div>
    </div>

</div>
<br>
<div class="row">

    <br>
    <br>
    <div class="col text-center">
    <button type="submit" class="btn btn-primary btnHorarios"  onClick="GravaAgendamento(<?php print $prof_id; ?> ,<?php print $espe_id; ?>)">Solicitar Horários</button>
    </div>
    
</div>