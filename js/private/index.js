
$(document).ready(function () {

    carregaEspecialidades();
})



function carregaEspecialidades() {

    $.ajax({
        url: "consulta.php?opcao=getEspecialidades&espe_id=",
        type: "GET",

        dataType: "html"

    }).done(function (resposta) {
        //   alert('Carregando Especialidades');
        //   console.log(resposta);
        console.log('Carregando Especialidades');
        var option;
        option += '<option>' + 'Selecione a especialidade' + '</option>';
        var obj = JSON.parse(resposta);
        console.log(obj['content']);
        obj['content'].forEach(function (o, index) {
            console.log(o.nome);
            option += '<option value =' + o.especialidade_id + '>' + o.nome + '</option>';
        });
        $('#cmbEspecialidades').html(option).show();



    }).fail(function (jqXHR, textStatus) {
        console.log("Erro ao pegar dados API " + textStatus);

    }).always(function () {
        console.log("Request Completo");
    });

}

function getMedicos() {

    var espe_id = $('#cmbEspecialidades').val();
    if (espe_id != 'Selecione a especialidade') {  //valida se pegou um id valido
       // alert(espe_id);
        $.ajax({
            url: "consulta.php?opcao=getProfessional&espe_id=" + espe_id,
            type: "GET",

            dataType: "html"

        }).done(function (resposta) {
           // alert('resposta');
                var option="";
            var obj = JSON.parse(resposta);
          //  console.log(obj['content']);
            obj['content'].forEach(function (o, index) {
            //    console.log('Nome: '+o.nome);
              //  console.log(o.nome);
              option += '<div class="col-xl-1" ></div>';
                option += '<div class="col-xl-4" id="dvmedicos">';
                option += '<div class="row">';
                option += ' <div class="areaImagem col-xl-4">';

                if (o.sexo == 'Feminino') {
                    option += '<img class="img" height="80" width="80" src="./imagens/femi.png" />';
                } else {
                    option += '<img class="img" height="80" width="80" src="./imagens/masc.png" />';
                }
    
                option += ' </div>';
                option += '<div class="areaTexto col-xl-8">';
                option += '<div class="row"><h4>' + o.tratamento + ' ' + o.nome +'<h4> </div>    ';
                option += '<div class="row"><h6>' + o.conselho + ' ' + o.documento_conselho + '<h6> </div> ';
                option += '<div class="row"> <button type="submit" class="btn btn-success btnAgendar" onClick="angendar('+o.profissional_id+','+espe_id+')" >Agendar</button></div>    ';
    
                option += '</div>';
                option += '</div>';
                option += '</div>';
                option += '<div class="col-xl-1" ></div>';




            });
            console.log(option);
            $('#dvprofe').html(option).show();

           





        }).fail(function (jqXHR, textStatus) {
            console.log("Erro ao pegar dados API " + textStatus);

        }).always(function () {
            console.log("Request Completo");
        });
    }


}
function angendar(profissional_id, especialidad_id){
//alert(profissional_id, especialidad_id);
window.location.href = "novoagendamento.php?prof_id="+profissional_id+"&espe_id="+especialidad_id;
}