
$(document).ready(function () {

    carregaOrigens();
})



function carregaOrigens() {

    $.ajax({
        url: "consulta.php?opcao=getOrigens&espe_id=",
        type: "GET",

        dataType: "html"

    }).done(function (resposta) {
        //   alert('Carregando Especialidades');
        //   console.log(resposta);
        console.log('Carregando Especialidades');
        var option;
        option += '<option>' + 'Como Conheceu?' + '</option>';
        var obj = JSON.parse(resposta);
        console.log(obj['content']);
        obj['content'].forEach(function (o, index) {
            console.log(o.nome_origem);
            option += '<option value =' + o.origem_id + '>' + o.nome_origem + '</option>';
        });
        $('#cmbConheceu').html(option).show();



    }).fail(function (jqXHR, textStatus) {
        console.log("Erro ao pegar dados API " + textStatus);

    }).always(function () {
        console.log("Request Completo");
    });

}

function GravaAgendamento(profissional_id, especialidad_id) {
//alert('aqui'+profissional_id+'-'+especialidad_id);
var nome = $("#txtnome").val();
var cpf = $("#txtcpf").val();    
var source_id = $("#cmbConheceu").val();
var dt_nasc =FormataStringData( $("#txtnascimento").val());

//alert(nome +' - '+cpf+' - '+source_id+' - '+dt_nasc);
$.post("./modelo/crud.php", {
    prof_id: profissional_id,
    espe_id: especialidad_id,
    nome: nome,
    cpf: cpf,
    source_id: source_id,
    dt_nasc:dt_nasc


}, function (data, status) {
  //  alert('resposta '+data);
if (data=='1') {
        //mostrar mensagem de sucesso
    $('#modal_exito').modal('show');
    $('#modal_error').modal('hide');
    $('#modal_preencher').modal('hide');
    $('#modal_origem').modal('hide');

} else {
    if (data==0) {
        $('#modal_error').modal('show');
        $('#modal_exito').modal('hide');
        $('#modal_preencher').modal('hide');
        $('#modal_origem').modal('hide');
       
    }
    if (data==2) {
        $('#modal_preencher').modal('show');
        $('#modal_exito').modal('hide');
        $('#modal_error').modal('hide');
        $('#modal_origem').modal('hide');
       
    }
    if (data==3) {
        $('#modal_origem').modal('show');
        $('#modal_exito').modal('hide');
        $('#modal_error').modal('hide');
        $('#modal_preencher').modal('hide');
       
    }
}
    // limpar campos
    $("#txtnome").val("");
    $("#txtcpf").val("");
    $("#txtnascimento").val("");  

  
});


}
function FormataStringData(data) {
    var dia  = data.split("/")[0];
    var mes  = data.split("/")[1];
    var ano  = data.split("/")[2];
  
    return ano + '-' + ("0"+mes).slice(-2) + '-' + ("0"+dia).slice(-2);
    // Utilizo o .slice(-2) para garantir o formato com 2 digitos.
  }
